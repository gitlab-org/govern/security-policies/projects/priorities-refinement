# Priorities Refinement

This project contains automation related to process described in https://about.gitlab.com/handbook/engineering/development/sec/govern/sp-ti-planning.html#epic-engineering-dri.

## How to configue?

1. Fork this project
1. In Project's Settings -> Access Token create token with Reporter role and `api` scope and create `GITLAB_TOKEN` variable with it's value in Settings -> CI/CD -> Variables (mark variable as masked and protected).
1. Set `PM_HANDLE` to GitLab handle of your PM (with `@`), `EM_HANDLE` to GitLab handle of your EM (with `@`), and `PRIORITIES_URL` set to the raw file with YAML priorities (ie. `https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/product_priorities/security_policies.yml`).
1. Go to CI/CD -> Schedules, create new schedule (weekly) and add variable `CREATE_PRIORITIES_AUTOMATION_NOTE` set to `true`.
