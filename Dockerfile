FROM ruby:3.2-alpine

RUN apk add --no-cache glab

RUN mkdir /gitlab
WORKDIR /gitlab

COPY automation.rb ./

SHELL ["/bin/bash", "-c"]
CMD ["ruby", "automation.rb"]

